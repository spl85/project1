import socket as soc, pickle
import sys

def get_TS_hostname(dns_table):
	for entry in dns_table:
		(host, record) = dns_table[entry]
		if(record == 'ns'):
			return host
	return 'no host'

if(len(sys.argv) != 2):
	print('Please invoke this server with \'python rs.py [rslistenport]\'')
	sys.exit()

#read the dns table into a dictionary of key/value pairs where key is hostname, and value is a tuple of (IP, Type)
#Type here is what the record type of the host is for example NS or A; where A is direct hit, NS is a nameserver address
dns_table = {}
dns = list()
with open('PROJI-DNSRS.txt') as f:
	dns = filter(lambda x : x != '', map(lambda x : x.lower().split('\r')[0], f.readlines()))
for server in dns:
	dns_table[server.split()[0]] = (server.split()[1], server.split()[2])
TS_hostname = get_TS_hostname(dns_table)
if(not bool(dns_table) or TS_hostname == 'no host'):
        print('The RSDNS table lacks a TS hostname, or the table is empty')
        sys.exit()

try:
	ss = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
except soc.error:
	print('Server socket creation failed')
#bind server to ip and port of client
rslistenport = int(sys.argv[1])
server_binding = ('', rslistenport)
try: ss.bind(server_binding)
except soc.error:
	print('Could not bind to client, binding address is already in use')
	sys.exit()
ss.listen(1)
print('Listening on port#: ' + str(rslistenport))
csock, caddr = ss.accept()
print('Successfully connected to client at ' + csock.getpeername()[0])

#receive queries from client
#get the number of queries first
num_queries = int(csock.recv(sys.getsizeof(int)))
csock.send('size received')
queries = list()
for i in range(0, num_queries, 1):
	queries.append(csock.recv(200))
	csock.send(str(i) + 'received')
	#check dns table for recently received query
	entry = dns_table.get(queries[i])
	if(entry is not None):
		#send the a record
		csock.send(pickle.dumps(entry))
	else:
		#dont have a record for the query, so send the TS hostname
		csock.send(pickle.dumps((TS_hostname, 'ns')))	
csock.close()
ss.close()

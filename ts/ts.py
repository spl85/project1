import socket as soc, pickle
import sys

if(len(sys.argv) != 2):
        print('Please invoke this server with \'python ts.py [tslistenport]\'')
        sys.exit()

#read the dns table into a dictionary of key/value pairs where key is hostname, and value is a tuple of (IP, Type)
#Type here is what the record type of the host is for example NS or A; where A is direct hit, NS is a nameserver address
dns_table = {}
dns = list()
with open('PROJI-DNSTS.txt') as f:
	dns = filter(lambda x : x != '', map(lambda x : x.lower().split('\r')[0], f.readlines()))
for server in dns:
        dns_table[server.split()[0]] = (server.split()[1], server.split()[2])
if(not bool(dns_table)):
        print('The TSDNS table is empty, exiting...')
        sys.exit()

#socket setup
try:
        ss = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
except soc.error:
        print('Server socket creation failed')
tslistenport = int(sys.argv[1])
try: ss.bind(('', tslistenport))
except soc.error:
	print('Could not bind to client, binding address is already in use')
	sys.exit()
print('Listening on port # ' + str(tslistenport))
#server handle loop
while(True):
	ss.listen(1)
	csock, caddr = ss.accept()
	query = csock.recv(200)
	if(query == 'quit_code_359'):
		csock.close()
		break	
	entry = dns_table.get(query)
	if(entry is not None):
		csock.send(pickle.dumps(entry))
	else:
		#dont have record, so send a placeholder null value
		csock.send(pickle.dumps(('-', '-')))
	csock.close()
ss.close()
sys.exit()	

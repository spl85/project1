Team: Stefan Lovric (spl85), Prad Rao (pr364)

---

Recursive Client Functionality

Description: Client connects to RS first and sends queries one by one. RS responds with an A record or NS record. If the RS responds with an A record, the record is then written to 'RESOLVED.txt', otherwise if an NS record is received, the client connects to the TS server whose hostname is provided in the NS record. The TS is then queried with the same hostname. If the hostname is found in the TSDNS, the TSDNS will respond with the A record of that query, and the client will write to 'RESOLVED.txt', otherwise if the query is not found in the TSDNS either, the client will write a host not found line into 'RESOLVED.txt'

---

Known issues in code?

Description: Not that we know of

---

Problems with developing the code?

Description: Not many problems, however first trying to figure out how to communicate with the TSDNS server with new sockets each time stopped us in our tracks slightly.

---

What did you learn by working on this project?  

Description: Acquired a good grasp of the programmatic client/rs/dns server interaction; definition of 'recursive' and 'iterative' querying became clear. Had a lot of fun coding this due to the power of Python.

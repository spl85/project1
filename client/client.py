import sys, os.path as path, socket as soc, pickle

if(len(sys.argv) != 4):
	print('Please supply [rshostname] [rslistenport] [tslistenport] to the program arguments')
	sys.exit()

#read query host name strings from PROJI-HNS.txt
hns = list()
with open('PROJI-HNS.txt', 'r') as f:
	hns = map(lambda x : x.split('\r')[0], f.read().lower().split('\n'))
	hns = filter(lambda x : x, hns)
try:
	cs = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
except soc.error:
	print('Error creating socket')
	sys.exit()
rshostname = sys.argv[1]
rslistenport = int(sys.argv[2])
tslistenport = int(sys.argv[3])
try: cs.connect((soc.gethostbyname(rshostname), rslistenport))
except soc.error:
        print('Error connecting to ' + rshostname)
	sys.exit()

#we are connected to the server, now we send each host name query to the server and await its response
print('Successfully connected to ' + str(rshostname) + ' at port#: ' + str(rslistenport))
#send server number of queries
cs.send(str(len(hns)))
cs.recv(len('size received'))
f = open('RESOLVED.txt', 'w')
ts_hostname = ""
for i in range(len(hns)):
	cs.send(hns[i])
	cs.recv(len(str(i) + 'received'))
	#receive the query response from server
	bits = cs.recv(4096)
	(ip, record) = pickle.loads(bits)
	#print(str((ip, record)))
	#if it is an 'A' record, save to 'RESOLVED.txt', otherwise connect to TSDNS and query
	if(record == 'a'):
		f.write(hns[i] + ' ' + ip + ' ' + record.upper() + '\n')	
	elif(record == 'ns'):
		ts_soc = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
		ts_hostname = ip
		try: ts_soc.connect((ip, tslistenport))
		except soc.error:
			print('Error connecting to TSDNS')
			sys.exit()
		ts_soc.send(hns[i])
		bits = ts_soc.recv(4096)
		(ip, record) = pickle.loads(bits)
		if(ip == '-'):
			#host not found
			f.write(hns[i] +  ' - ' + 'Error:HOST NOT FOUND\n')
		else:
			f.write(hns[i] + ' ' + ip + ' ' + record.upper() + '\n')
		ts_soc.close()
ts_soc_final = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
try: ts_soc_final.connect((ts_hostname, tslistenport))
except soc.error:
	print('Error connecting with final socket to TSDNS')
	sys.exit()
ts_soc_final.send('quit_code_359')
ts_soc_final.close()
f.close()
cs.close()	
